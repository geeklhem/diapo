""" main.py -- Command line interface
This File is part of the diapo package
2019 Guilhem Doulcier, GNU GPLv3+

Notes for later:

TODO:
- `diapo init`, create an empty md
"""

import sys
import argparse
import logging
import xml.etree.ElementTree as et
import subprocess
import os
import hashlib
import json

logger = logging.getLogger('diapo')
logger.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
console_handler.setFormatter(logging.Formatter('%(levelname)-7s  %(message)s'))
logger.addHandler(console_handler)

APP_PATH = os.path.dirname(os.path.abspath(__file__))

# Register the SVG namespace.
et.register_namespace('', "http://www.inkscape.org/namespaces/inkscape")
et.register_namespace('',"http://www.w3.org/2000/svg")

def get_layers(root):
    """ Get the list of layers """
    return [child for child in root if child.tag[-1]=='g']

def get_label(layer):
    """ Get the label of a layer """
    try:
        return layer.attrib['{http://www.inkscape.org/namespaces/inkscape}label']
    except Exception as ex:
        print("ERROR getting layer attrib")
        print(ex)
        print(layer)
        return 'none'

def show_only(layers, show_labels):
    """ Show only the layers in the `show_labels` list """
    for l in layers:
        if get_label(l) not in show_labels:
            l.attrib['style']='display:none'
        else:
            l.attrib['style']='display:inline'

def load_svg(path):
    """ Load an svg file from path with elementtree
    (Create an empty slide if the file does not exist)."""

    if os.path.exists(path):
        svg = et.parse(path)
    else:
        logger.warning("File {} not found !".format(path))
        svg = et.ElementTree(et.fromstring('<svg viewBox="0 0 1080 720"> </svg>'))
    root = svg.getroot()

    hashsvg = hashlib.md5(et.tostring(root)).hexdigest()


    layers = get_layers(root)
    lnames = [get_label(l) for l in layers]
    logger.debug('FILE: {}, {} layers ({}).'.format(path, len(layers), ', '.join(lnames)))
    return {'svg':svg,'root':root, 'layers':layers, "labels":lnames, 'hash':hashsvg}

def add_slide_number(root, number, maxnb):
    """Add the slide_number element that will be used in the pdf export to display slide number"""

    # Get slide size
    x0,y0,w,h = [float(x) for x in root.attrib['viewBox'].split(' ')]

    # Create the element
    nb_txt = r'<text x="{}px" y="{}px" text-anchor="end" fill="black" opacity="0.75" id="slide_nb" style="font: bold 4em sans-serif">{}/{}</text>'.format(w-15,h-10, number, maxnb)
    slide_nb = et.fromstring(nb_txt)

    # Add it to the svg.
    root.append(slide_nb)

def compose_html(body, additional_js=None):
    """ Concatenate all the html+js in a self contained file"""
    with open(os.path.join(APP_PATH, 'static/template.html'), 'r') as file:
        head, foot = file.read().split('<!--- SLIDES !--->')
    with open(os.path.join(APP_PATH, 'static/lib/main.css'), 'r') as file:
        css = file.read()
    with open(os.path.join(APP_PATH, 'static/lib/reveal.js'), 'r') as file:
        revealjs = file.read()
    foot = foot.replace('<!--- scripts !--->', '{}<script>{}</script>\n<style>{}</style>'.format(additional_js, revealjs, css))
    return head + body + foot

def parse_in(file):
    """ Parse the slide description file in the text (.in) format. """
    slides = []
    with open(file,'r') as f:
        for line in f.readlines():
            svg_path = line.split(':',1)[0].strip()
            layers = ":".join(line.split(':')[1:]).split("#",1)[0].split(',')
            comment = None if "#" not in line else line.split("#",1)[1]
            layers = [l.strip() for l in layers]
            slides.append((svg_path, layers, comment))
        return slides

def parse_md(file):
    """ Parse the slide description file in the markdown format """
    slides = []
    with open(file,'r') as f:
        for line in f.readlines():
            if line[0] == "!":
                svg_path = line.split(')',1)[0].split('(',1)[1].strip()
                layers = line.split('layers="',1)[1].split('"',1)[0].strip()
                comment = ''
                layers = [l.strip() for l in layers.split(',')]
                slides.append((svg_path, layers, comment))
    return slides

def pdf_export_slide(hashframe, cache_folder, data, previous, name, slide_number, slide_max_number, cache):
    """ Export an individual frame as pdf """
    fname = os.path.join(cache_folder, '{}.svg'.format(hashframe))

    if previous != name:
        add_slide_number(data[name]['root'], slide_number, slide_max_number)

    if not cache or not os.path.exists(fname):
        data[name]['svg'].write(fname)
        args = 'inkscape {} -T -o {}'.format(fname, fname.replace('svg', 'pdf'))
        logger.debug(f'Export slide {name} to pdf using inkscape.')
        try:
            subprocess.check_output(args, shell=True)
        except subprocess.CalledProcessError as ex:
            logger.error(f"Slide export with inkscape failed: {ex}")

    else:
        logger.debug('Slide exists in cache.')
    return data, fname

def pdf_concatenate_slides(outdir, outfile_name, fnames, markdown, notes, compress, nogenericname=True):
    outfile = os.path.join(outdir, outfile_name+'.pdf')
    if outfile_name == "slides" and  nogenericname:
        outfile_name = os.path.basename(os.path.dirname(os.path.abspath(outfile)))
        logger.info(f"'slides.pdf' is too generic a name, renaming the output to {outfile_name}.pdf ")
    outfile = os.path.join(outdir, outfile_name+'.pdf')
        
        
    # Concatenate all the files using pdftk.
    logger.info('PDF concatenation to {}'.format(outfile))
    args = 'pdftk {} cat output {}'.format(' '.join(fnames).replace('svg', 'pdf'), outfile)
    try:
        subprocess.check_output(args, shell=True)
    except subprocess.CalledProcessError as ex:
        logger.error(f"pdftk concatenation failed: {ex}")

    if compress:
        logger.info('Compress the output')
        args = f"gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile={outfile.replace('.pdf', '_compressed.pdf')} {outfile}"
        try:
            subprocess.check_output(args, shell=True)
        except subprocess.CalledProcessError as ex:
            logger.error(f"Compression with gs failed: {ex}")

    if markdown and notes:
        logger.info('Running pandoc for slides notes')
        notefile = outfile.replace('.pdf','_outline.pdf')
        logger.warning("FIXME: the name of the file is hardcoded")
        args = 'pandoc {} -o {}'.format("slides.md", notefile)
        try:
            subprocess.check_output(args, shell=True)
        except subprocess.CalledProcessError as ex:
            logger.error(f"Pandoc outline failed: {ex}")
        if compress:
            args = f"gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile={notefile.replace('.pdf', '_compressed.pdf')} {notefile}"
            try:
                subprocess.check_output(args, shell=True)
            except subprocess.CalledProcessError as ex:
                logger.error(f"Compression with gs failed: {ex}")


def main(file, fmt, outdir, outfile_name, cache_folder, cache, notes, compress):
    # Parse the input.
    markdown = False

    try:
        if file.split('.')[-1] == 'in':
            logger.debug(f"Parse the input {file} (text format)")
            slides = parse_in(file)
        if file.split('.')[-1] == 'md':
            logger.debug(f"Parse the input {file} (markdown format)")
            slides = parse_md(file)
            markdown = True
    except FileNotFoundError:
        logger.error(f"File {file} not found.")
        sys.exit("File not found")

    # Load the svg files.
    logger.debug(f"Load the data for {len(slides)} slides")
    data = {}
    for d,_,_ in slides:
        if d not in data:
            data[d] = load_svg(d)

    fnames = []
    slide_xml = {}
    slide_number = 0
    previous = None
    frame_progression = {}

    # Prepare the cache.
    if not os.path.exists(cache_folder):
        os.mkdir(cache_folder)

    slide_max_number = 0
    for i, (name, layers, comment) in enumerate(slides):
        if previous != name:
            slide_max_number +=1
        previous = name
    previous = None
    logger.debug(f"The presentation will contain {slide_max_number} slides")

    # Export individual slides.
    for i, (name, layers, comment) in enumerate(slides):

        # Each svg slide is loaded and associated with a hash of its content for caching.
        # We add the hash of the frame layers to get a complete hash.
        hashframe = (data[name]['hash']
                     +'_'+hashlib.md5(str(sorted(layers)).encode()).hexdigest()
                     +f'_{slide_number}')

        # Put all layers to visible.
        show_only(data[name]['layers'], layers)

        # Get frame number and slide number.
        # Frame number change every page, while slide number only changes
        # when the svg change.
        if previous != name:
            slide_number +=1

        logger.info(f'SLIDE: {slide_number:2} (Page {i:3}) ({",".join(layers)})')

        if fmt=='pdf':
            data, fname = pdf_export_slide(hashframe, cache_folder, data, previous, name, slide_number, slide_max_number, cache)
            fnames.append(fname)
        elif fmt=='js':
            data, slide_xml, frame_progression = js_export_slide(data, slide_xml, frame_progression)
        previous = name

    if fmt=='pdf':
        pdf_concatenate_slides(outdir, outfile_name, fnames, markdown, notes, compress)
    elif fmt=='js':
        js_concatenate_slides(frame_progression)

def js_export_slide(data, slide_xml, frame_progression):
    if slide_number not in slide_xml:
        data[name]['root'].attrib['class'] = 'stretch' #CSS for reveal.js.
        slide_xml[slide_number] = et.tostring(data[name]['root'], encoding='unicode')
        frame_progression[slide_number] = [layers,]
    else:
        frame_progression[slide_number].append(layers)
    return data, slide_xml, frame_progression

def js_concatenate_slides():
    # Concatenate all xml slides.
    # Each slide is a svg, plus a invisible fragment element per frame.
    # The fragments are used by the slide loader.js
    # to hide and display the layers when the button is pressed.
    body = []
    slide_loader = []
    for slide_number, progress in sorted(frame_progression.items()):
        slide_name = 'slide{}'.format(slide_number)
        body.append('{}'.format('<section id="{}">{}'.format(slide_name, slide_xml[slide_number])))
        body.append('\n'.join(['<span class="fragment"></span>']*len(progress)))
        body.append('</section>')
        slide_loader.append('SLIDE_HOOKS["{}"] = svg_progressive({})'.format(slide_name, json.dumps(progress)))
    script = "<script>var SLIDE_HOOKS = {{}}; {}</script>".format('\n'.join(slide_loader))
    page = compose_html('\n'.join(body), script)
    with open('index.html','w') as file:
        file.write(page)


def cli():
    '''Command line interface'''
    parser = argparse.ArgumentParser(description='Diapo - Generate a slideshow from SVG')
    parser.add_argument('input', help='Input file', nargs='?', default='slides.in')
    parser.add_argument('--outdir', help='Ouput directory', default='.', nargs="?")
    parser.add_argument('--outfile', help='Ouput filename',  nargs="?")
    parser.add_argument('--format', help='Output format (js,pdf)', default='pdf', nargs="?")
    parser.add_argument('--cache_folder', help='Cache folder', default='.slides', nargs="?")
    parser.add_argument('--no_cache', help='Ignore cached data.', action='store_true')
    parser.add_argument('--notes', help='Export markdown notes', action='store_true')
    parser.add_argument('--watch', help='Uses entr to watch the changes and rerun diapo', action="store_true")
    parser.add_argument('--compress', help="Compress result", action='store_true')

    args = parser.parse_args(sys.argv[1:])

    if args.watch:
        try:
            args = "ls slides.md *.svg | entr diapo " + " ".join(sys.argv[1:]).replace('--watch','')
            subprocess.check_output(args, shell=True)
        except subprocess.CalledProcessError as ex:
            logger.error(f"Slide export with inkscape failed: {ex}")
        exit()

    logger.debug("Diapo Started")
    logger.debug(f"FILE: {args.input}")

    if args.outfile is None:
        args.outfile = os.path.basename(args.input.split('.')[0])
    
    main(file=args.input, fmt=args.format, outfile_name=args.outfile, outdir=args.outdir,
         cache_folder=args.cache_folder, cache=(not args.no_cache), notes=args.notes,
         compress=args.compress)
