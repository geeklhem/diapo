# Diapo - Assemble a presentation from svg slides.

If you are like me and like to prepare your presentation within
inkscape, but are tired of exporting each slide manually while
fiddling with layer opacity, Diapo might be for you as a dead simple,
no-nonsense way to get a pdf.

This script takes a simple input where each line defines a slide with a
path to an svg file and a list of inkscape layers to display. It will
export all slides as pdf using inkscape and concatenate the result
using pdftk. It will also add slide number and speaker annotations. 

## Installation

If you have Python 3 along with `pip`, installing the current
development version of `diapo` can be done by:

```
pip install https://gitlab.com/geeklhem/diapo/repository/archive.zip?ref=master
```

## Quick Start

The `demo` folder contain an example of presentation. It can be compiled by executing:

```
diapo
```

in ths folder. By default, slide descriptions are loaded from a
`slides.in` file. Each line define a new frame (or page in the PDF) and must be formatted as:

```
relative_path_to_svg: layer1, layer2, layer3 # Optional comment
```

When the pdf is produced, the only objects that are displayed are
the inkscape layers that are defined within the slides.in file
(`layer1, layer2, layer3` in the example). Slide number is added, and
is not increased if two consecutive lines share the same svg source file.

Alternatively, the slides can be described in a `slides.md` file, the sytax is:

```
![](relative_path_to.svg){layers="layer1 layer2 layer3"}
```

If you give the option `--notes` it will export the markdown file
using pandoc. So you can take notes and have a handout generated with
the same script than the one that generates the slides pdf. 


The option `--watch` keeps diapo running and regenerates the slides
when a file change.


## Todo

- Testing
- Better documentation
- Better demo 
- Fix the javascript export 

Feel free to contribute.

## Complete Dependencies

This software is written in Python 3 and uses the following software for pdf export:

- **[Inkscape](https://inkscape.org)**: SVG editor used to convert svg to PDF (debian package `inkscape`).
- **[pdftk](https://gitlab.com/pdftk-java/pdftk)**: Used to assemble the slides (debian package `pdftk`)
- **[entr](https://eradman.com/entrproject/)** Regenerate the slides with the `--watch` option (debian package `entr`)

(Experimental) HTML export uses the the following libraries (included in the `static/lib` directory):

- **[Revealjs](https://revealjs.com/)**: The javascript presentation framework.

## License

Copyright (c) 2019 Guilhem Doulcier

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

