from setuptools import setup

setup(name='diapo',
      version='0.1',
      description='Generate a slideshow from svg',
      url='https://gitlab.com/geeklhem/diapo',
      author='Guilhem Doulcier',
      author_email='guilhem.doulcier@ens.fr',
      license='GPLv3+',
      python_requires='>=3',
      packages=['diapo'],
      entry_points={
        'console_scripts': [
            'diapo = diapo.main:cli',
        ]},
      install_requires=[],
      zip_safe=False)
